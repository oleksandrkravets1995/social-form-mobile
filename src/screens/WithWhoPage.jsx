import React, { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import { useSelector } from "./../store/index";
import { UserDataActionCreator } from "./../store/test/actions";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";

export const WithWhoPage = () => {
  const { t } = useTranslation();
  const active = useSelector((state) => state.test.animation);
  const dispatch = useDispatch();
  const [btnId, setBtnId] = useState(0);
  console.log(btnId);
  return (
    <div className={["page", "personInfo-page"].join(" ")}>
      <div
        className="container"
        style={{
          display: "flex",
          flexDirection: "column",
          height: "100vh",
          justifyContent: "space-evenly",
        }}
      >
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <h2>
            {t("with_who")}
            {/* Fews Questions <pre />
          About you */}
          </h2>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-evenly",
              flexWrap: "wrap",
              width: "100%",
            }}
          >
            <div
              id="1"
              onClick={(e) => {
                setBtnId(e.currentTarget.id);
                dispatch(UserDataActionCreator.setWhithWho("family"));
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="113"
                height="99"
                viewBox="0 0 113 99"
              >
                <defs>
                  <filter
                    id="Rectangle_32"
                    x="0"
                    y="0"
                    width="113"
                    height="99"
                    filterUnits="userSpaceOnUse"
                  >
                    <feOffset dy="3" input="SourceAlpha" />
                    <feGaussianBlur stdDeviation="3" result="blur" />
                    <feFlood flood-opacity="0.161" />
                    <feComposite operator="in" in2="blur" />
                    <feComposite in="SourceGraphic" />
                  </filter>
                </defs>
                <g
                  id="Composant_2_1"
                  data-name="Composant 2 – 1"
                  transform="translate(9 6)"
                >
                  <g
                    transform="matrix(1, 0, 0, 1, -9, -6)"
                    filter="url(#Rectangle_32)"
                  >
                    <rect
                      id="Rectangle_32-2"
                      data-name="Rectangle 32"
                      width="95"
                      height="81"
                      rx="40"
                      transform="translate(9 6)"
                      fill="#be814e"
                    />
                  </g>
                  <text
                    id="Family"
                    transform="translate(22.5 44.5)"
                    fill={btnId === "1" ? "black" : "#fff"}
                    textDecoration={btnId === "1" ? "underline" : "none"}
                    font-size="16"
                    font-family="Eina01-Bold"
                    font-weight="700"
                  >
                    <tspan x="0" y="0">
                      {t("family")}
                    </tspan>
                  </text>
                </g>
              </svg>
            </div>
            <div
              id="2"
              onClick={(e) => {
                setBtnId(e.currentTarget.id);
                dispatch(UserDataActionCreator.setWhithWho("friends"));
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="112"
                height="99"
                viewBox="0 0 112 99"
              >
                <defs>
                  <filter
                    id="Rectangle_31"
                    x="0"
                    y="0"
                    width="112"
                    height="99"
                    filterUnits="userSpaceOnUse"
                  >
                    <feOffset dy="3" input="SourceAlpha" />
                    <feGaussianBlur stdDeviation="3" result="blur" />
                    <feFlood flood-opacity="0.161" />
                    <feComposite operator="in" in2="blur" />
                    <feComposite in="SourceGraphic" />
                  </filter>
                </defs>
                <g
                  id="Composant_3_1"
                  data-name="Composant 3 – 1"
                  transform="translate(9 6)"
                >
                  <g
                    transform="matrix(1, 0, 0, 1, -9, -6)"
                    filter="url(#Rectangle_31)"
                  >
                    <rect
                      id="Rectangle_31-2"
                      data-name="Rectangle 31"
                      width="94"
                      height="81"
                      rx="40"
                      transform="translate(9 6)"
                      fill="#be814e"
                    />
                  </g>
                  <text
                    id="Friends"
                    transform="translate(18.5 44.5)"
                    fill={btnId === "2" ? "black" : "#fff"}
                    textDecoration={btnId === "2" ? "underline" : "none"}
                    font-size="16"
                    font-family="Eina01-Bold"
                    font-weight="700"
                  >
                    <tspan x="0" y="0">
                      {t("friends")}
                    </tspan>
                  </text>
                </g>
              </svg>
            </div>
            <div
              id="3"
              onClick={(e) => {
                setBtnId(e.currentTarget.id);
                dispatch(UserDataActionCreator.setWhithWho("others"));
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="112"
                height="99"
                viewBox="0 0 112 99"
              >
                <defs>
                  <filter
                    id="Rectangle_53"
                    x="0"
                    y="0"
                    width="112"
                    height="99"
                    filterUnits="userSpaceOnUse"
                  >
                    <feOffset dy="3" input="SourceAlpha" />
                    <feGaussianBlur stdDeviation="3" result="blur" />
                    <feFlood flood-opacity="0.161" />
                    <feComposite operator="in" in2="blur" />
                    <feComposite in="SourceGraphic" />
                  </filter>
                </defs>
                <g
                  id="Composant_4_1"
                  data-name="Composant 4 – 1"
                  transform="translate(9 6)"
                >
                  <g
                    transform="matrix(1, 0, 0, 1, -9, -6)"
                    filter="url(#Rectangle_53)"
                  >
                    <rect
                      id="Rectangle_53-2"
                      data-name="Rectangle 53"
                      width="94"
                      height="81"
                      rx="40"
                      transform="translate(9 6)"
                      fill="#be814e"
                    />
                  </g>
                  <text
                    id="Others"
                    transform="translate(21.5 44.5)"
                    fill={btnId === "3" ? "black" : "#fff"}
                    textDecoration={btnId === "3" ? "underline" : "none"}
                    font-size="16"
                    font-family="Eina01-Bold"
                    font-weight="700"
                  >
                    <tspan x="0" y="0">
                      {t("others")}
                    </tspan>
                  </text>
                </g>
              </svg>
            </div>
          </div>
        </CSSTransition>
      </div>
    </div>
  );
};
