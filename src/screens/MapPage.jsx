import React from "react";
import { useSelector } from "./../store/index";
import { CSSTransition } from "react-transition-group";
import { useTranslation } from "react-i18next";

export const MapPage = () => {
  const active = useSelector((state) => state.test.animation);
  const { t } = useTranslation();
  return (
    <div className={["page", "map-page"].join(" ")}>
      <div className="container">
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <h2>
            {t("where_is_it")}
            {/* Where is it located <pre />
          on the map? */}
          </h2>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div className="map-wrapper"></div>
        </CSSTransition>
      </div>
    </div>
  );
};
