import { showError } from "../utils/message";
import { request, requestToken } from "../utils/request";

/*
{
  "variety": "مبروم",
  "quantity": 500,
  "farmer": {
      "firstName": "Pavlo",
      "lastName": "TestFarmer",
      "certification": "الممارسات الزراعية الجيدة السعودية",
      "agroTourism": false,
      "photos": []
  }
}
 */
export const getFarmerLot = (userID, lotId) => async (dispatch) => {
  if (!userID || !lotId) {
    return dispatch(errorAction());
  }
  try {
    const {access_token} = await requestToken();

    const data = await request(`/api/v1/values/lots/${lotId}/farmer?userId=${userID}`, {token: access_token});
    dispatch({ payload: data, type: 'SET_LOT' });
  } catch (err) {
    dispatch(errorAction());
  }
};

//{visitorId: 1, age: 23, nationality: "Some", firstTime: false, beenHereBefore: 4, beenWith: "Friends", lovedThings: "Travelling", happinessLevel: "Good"}
export const saveSurveyData = () => async (dispatch, getState) => {
  const data = getState().test.userdata;
  data.visitorId = Date.now();
  try {
    await request("/api/v1/public/survey", {method: "POST", body: data});
    dispatch({ type: 'SET_SAVE_SUCCESS' });
  } catch (err) {
    showError('Failed to save. Try again');
  }
};

const errorAction = () => ({ type: 'SET_GET_LOT_ERROR' });
