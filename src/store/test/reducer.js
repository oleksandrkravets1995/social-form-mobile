const initialState = {
	test: 0,
	animation: true,
	userdata: {
		age: 0,
		nationality: '',
		firstTime: false,
		beenHereBefore: 0,
		beenWith: '',
		lovedThings: '',
		ideas: '',
		happinessLevel: 0,
	}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'SET_TEST': {
			return {
				...state,
				test: action.payload,
			};
		}
		case 'SET_ANIM': {
			return {
				...state,
				animation: action.payload
			}
		}
		case 'SET_DATA_AGE': {
			return {
				...state,
				userdata: {
					...state.userdata,
					age: action.payload
				}
			}
		}
		case 'SET_DATA_NATIONALITY': {
			return {
				...state,
				userdata: {
					...state.userdata,
					nationality: action.payload
				}
			}
		}
		case 'SET_DATA_HAVE_VISITED': {
			return {
				...state,
				userdata: {
					...state.userdata,
					firstTime: !action.payload
				}
			}
		}
		case 'SET_DATA_VISIT_NUM': {
			return {
				...state,
				userdata: {
					...state.userdata,
					beenHereBefore: action.payload
				}
			}
		}
		case 'SET_DATA_WITH_WHO': {
			return {
				...state,
				userdata: {
					...state.userdata,
					beenWith: action.payload
				}
			}
		}
		case 'SET_DATA_MOST_LOVED': {
			return {
				...state,
				userdata: {
					...state.userdata,
					lovedThings: action.payload
				}
			}
		}
		case 'SET_DATA_IDEAS': {
			return {
				...state,
				userdata: {
					...state.userdata,
					ideas: action.payload
				}
			}
		}
		case 'SET_DATA_HAPPINES': {
			return {
				...state,
				userdata: {
					...state.userdata,
					happinessLevel: action.payload
				}
			}
		}
		default:
			return state;
	}
};

export default reducer;
