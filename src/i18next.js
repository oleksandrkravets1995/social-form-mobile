import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';

const resources = {
  en: {
    translation: {
      "hello": "Hello!",
      "welcome_text": "The Royal Commission for Alula is happy to welcome you to the 2021 date festival",
      "swipe_up": "Swipe up",
      "dates_sold": "Dates sold",
      "mohamed_al_mallouhi": "Mohamed AL Mallouhi",
      "barni": "Barni",
      "certified_organic": "Certified organic",
      "farmer_parctices": "The farmer practices agritourism.",
      "where_is_it": "Where is it located on the map?",
      "your_opinion": "Your opinion matters",
      "few_questions": "Few Questions About you",
      "how_old": "How old are you?",
      "ans": "ans",
      "nationality": "Nationality",
      "been_here": "Have you been here before",
      "oui": "Oui",
      "non": "Non",
      "with_who": "With who?",
      "family": "Family",
      "friends": "Friends",
      "others": "Others",
      "what_is_the_thing": "What is the thing that you loved the most?",
      "music": "Music",
      "food": "Food",
      "products": "Products",
      "activities": "Activities",
      "ideas_festival": "Ideas to improve the festival?",
      "how_happy": "How happy you are?",
      "share_opinion": "Share my opinion",
      "thanks_for_info": "Thanks for these information! They will help us <br/> to improve the experience of the Al Ula festival.",
      "your_welcome": "Your welcome",
      "thanks_for_time": "Thank you for your time & enjoy your time at the festival",
      "thanks_for_coming": "Al Ula Dattes festival thanks you for coming and for your attention.",
      "chose": "Please choose what you liked the most",
      "please_select_nat": "Please select your nationality",
      "please_answer": "Please answer all questions"
    }
  },
  ar: {
    translation: {
      "hello": "مرحباً",
      "welcome_text": "الهيئة الملكية في العلا ترحب بكم في مهرجان التمور ٢٠٢١",
      "swipe_up": "اسحب للأعلى",
      "dates_sold": "مبيعات التمور",
      "mohamed_al_mallouhi": "محمد الملوحي",
      "barni": "برني",
      "certified_organic": "شهادة الزراعة العضوية",
      "farmer_parctices": "مزارع ممارس للسياحة الزراعية",
      "where_is_it": "اين تقع على الخريطة؟?",
      "your_opinion": "رأيك يهمنا",
      "few_questions": "أسئلة قليلة عنك",
      "how_old": "كم عمرك؟",
      "ans": "السنوات",
      "nationality": "الجنسية",
      "been_here": "هل اتيت الى هنا من قبل؟",
      "oui": "نعم",
      "non": "لا",
      "with_who": "مع من؟",
      "family": "العائلة",
      "friends": "الأصدقاء",
      "others": "اخرون",
      "what_is_the_thing": "ماهو اكثر شيء اعجبك؟",
      "music": "الموسيقى",
      "food": "الاكل",
      "products": "المنتجات",
      "activities": "الفعاليات",
      "ideas_festival": "أفكار لتحسين المهرجان؟",
      "how_happy": "مامدى سعادتك؟",
      "share_opinion": "شاركنا رأيك",
      "thanks_for_info": "نشكرك على هذه المعلومات والتي ستساعدنا في تحسين التجربة المهرجان العلا.",
      "your_welcome": "اهلا و سهلا",
      "thanks_for_time": "شكرا لك لاعطائنا جزء من وقتك, استمتع بوقتك في المهرجان",
      "thanks_for_coming": "مهرجان العلا للتمور يشكرك لحضورك واهتمامك",
      "chose": "رجائا, اختر اكثر شيء اعجبك",
      "please_select_nat": "نرجو اختيار جنسيتك",
      "please_answer": "رجائا, اجب على جميع الاسئلة"
    }
  }
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    supportedLngs: ['ar', 'en'],
    fallbackLng: 'ar',
    detection: {
      order: ['path'],
    },
  });

export default i18n;