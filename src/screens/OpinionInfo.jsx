import React from "react";
import { useSelector } from "./../store/index";
import { CSSTransition } from "react-transition-group";
import { useTranslation } from "react-i18next";

export const OpinionInfo = () => {
  const active = useSelector((state) => state.test.animation);
  const { t } = useTranslation();

  return (
    <div className={["page", "opinion-info-page"].join(" ")}>
      <CSSTransition in={active} classNames="from_left" timeout={300}>
        <div className="circle">
          <p>{t("your_opinion")}</p>
        </div>
      </CSSTransition>
      <CSSTransition in={active} classNames="from_right" timeout={300}>
        <div className="arrow">
          <svg
            width="24px"
            height="24px"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M6 12H18.5M18.5 12L12.5 6M18.5 12L12.5 18"
              stroke="rgba(88 70 54)"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        </div>
      </CSSTransition>
    </div>
  );
};
