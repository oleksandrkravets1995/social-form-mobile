import { combineReducers } from 'redux';
import { useSelector as useReduxSelector } from 'react-redux';
// import { ThunkDispatch } from 'redux-thunk';
import testReducer from './test/reducer';
import { lotReducer, successReducer, errorReducer } from './reducers';

const rootReducer = combineReducers({
  lot: lotReducer,
  error: errorReducer,
  success: successReducer,
	test: testReducer
});

export const useSelector = useReduxSelector;

export default rootReducer;
