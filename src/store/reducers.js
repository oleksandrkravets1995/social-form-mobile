export const successReducer = (state = false, action) => {
  switch (action.type) {
    case 'SET_SAVE_SUCCESS': {
      return true;
    }
    default: {
      return state;
    }
  }
};

export const errorReducer = (state = false, action) => {
  switch (action.type) {
    case 'SET_GET_LOT_ERROR': {
      return true;
    }
    default: {
      return state;
    }
  }
};

export const lotReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_LOT': {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};
