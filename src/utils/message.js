// import { store } from 'react-notifications-component';

// export const showError = (message) => {
//   store.addNotification({
//     animationIn: [ 'animated', 'fadeIn' ],
//     animationOut: [ 'animated', 'fadeOut' ],
//     container: 'top-right',
//     dismiss: {
//       duration: 5000,
//       // onScreen: true,
//       pauseOnHover: true
//     },
//     insert: 'top',
//     message,
//     title: 'Error!',
//     type: 'danger'// 'success' | 'danger' | 'info' | 'default' | 'warning'
//   });
// };

// export const showSuccess = (message) => {
//   store.addNotification({
//     animationIn: [ 'animated', 'fadeIn' ],
//     animationOut: [ 'animated', 'fadeOut' ],
//     container: 'top-right',
//     dismiss: {
//       duration: 5000,
//       pauseOnHover: true
//     },
//     insert: 'top',
//     message,
//     title: 'Success!',
//     type: 'success'
//   });
// };

export const showError = (message) => {
  alert('Error! ' + message);
};
