const hostname = window.location.hostname;
const subDomain = hostname.split('.')[0] === 'df21' ? 'agbox-datesfestival.' : '';
const baseUrl = `https://${subDomain}peoples.link:8080`;

export const request = async (url, {method = "GET", body = null, token}) => {
  try {
    const headers = {
      "Content-Type": "application/json",
    };
    if (token) {
      headers.Authorization = `Bearer ${token}`;
    }
    if (body) {
      body = JSON.stringify(body);
    }
    const resp = await fetch(baseUrl + url, { body, headers, method });
    if (!resp.ok) {
      const msg = resp.status + " Failed request to the server";
      const err = await resp
        .json()
        .then((data) => new Error(data.message || msg))
        .catch(() => new Error(msg));
      err.status = resp.status;
      throw err;
    }
    if (resp.status === 204) {
      return;
    }
    return resp.json();
  } catch (e) {
    console.log('REQUEST ERROR', e);
    throw e;
  }
};

export const requestToken = async () => {
  try {
    const formdata = new FormData();
    formdata.append('grant_type', 'password');
    formdata.append('scope', 'all');
    formdata.append('client id', 'web-app');
    formdata.append('client secret', 'secret');
    formdata.append('username', 'cms');
    formdata.append('password', 'Cms20&Wer}po');

    const headers = {
      Authorization: 'Basic d2ViLWFwcDpzZWNyZXQ=',
    };

    const url = baseUrl + "/oauth/token";

    const resp = await fetch(url, { body: formdata, headers, method: 'POST', redirect: 'follow' });
    if (!resp.ok) {
      const msg = resp.status + " Failed request to the server";
      const err = await resp
        .json()
        .then((data) => new Error(data.message || msg))
        .catch(() => new Error(msg));
      err.status = resp.status;
      throw err;
    }
    return resp.json();
  } catch (e) {
    console.log('REQUEST ERROR', e);
    throw e;
  }
};
