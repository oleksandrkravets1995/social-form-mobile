import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './i18next';
import { Provider } from 'react-redux';
import { initializeStore } from './store/init';


ReactDOM.render(
  <Provider store={initializeStore()}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);
