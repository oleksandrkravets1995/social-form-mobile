import React from "react";
import { useSelector } from "./../store/index";
import { CSSTransition } from "react-transition-group";
import { useTranslation } from "react-i18next";

export const ImagesInfo = () => {
  const active = useSelector((state) => state.test.animation);
  const { t } = useTranslation();
  return (
    <div className={["page", "map-page"].join(" ")}>
      <div className="container">
        <div className="images-block">
          <CSSTransition in={active} classNames="from_left" timeout={300}>
            <div className="images-block-column">
              <div className={["image-square", "brown"].join(" ")}>
                <h3 className="title">Mohammed Al Mallouhi</h3>
              </div>
              <div className={["image-square", "green"].join(" ")}>
                <h3 className="title">Barni</h3>
              </div>
            </div>
          </CSSTransition>
          <CSSTransition in={active} classNames="from_right" timeout={300}>
            <div className="images-block-column">
              <div className={["image-square", "orange"].join(" ")}>
                <h3 className="title">Dates sold</h3>
              </div>
              <div className={["image-square", "yellow"].join(" ")}>
                <h3 className="title">Certified organic</h3>
              </div>
            </div>
          </CSSTransition>
        </div>
      </div>
      <div style={{ margin: "60px 0 30px 0" }}>
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <div className="title-block">
            <p>The farmer practices agritourism.</p>
          </div>
        </CSSTransition>
      </div>
      <CSSTransition in={active} classNames="from_left" timeout={300}>
        <div className="images-together">
          <div className="images-item-together"></div>
          <div className="images-item-together"></div>
          <div
            className={["images-item-together", "center-image"].join(" ")}
          ></div>
        </div>
      </CSSTransition>
    </div>
  );
};
