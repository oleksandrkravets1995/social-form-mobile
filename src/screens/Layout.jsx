import { useSwipeable } from "react-swipeable";
import React, { useEffect, useState } from "react";
import {
  WelcomePage,
  MapPage,
  OpinionInfo,
  SuccessPage,
  HappyPage,
  HobbyPage,
  PersonInfoPage,
  ImagesInfo,
  WithWhoPage,
} from "./index";
import { useDispatch, useSelector } from "react-redux";
import { setAnim } from "./../store/test/actions"
import { getFarmerLot } from "../store/actions";

const Layout = () => {
  const [componentNum, setComponentNum] = useState(0);
  const [animationDirectionClass, setAnimationDirectionState] = useState("");
  const dispatch = useDispatch();
  const error = useSelector(state => state.error);
  const success = useSelector(state => state.success);
  const swipe = useSwipeable({
    onSwipedUp: () => {
      if (componentNum !== 8) {
        setAnimationDirectionState("forward");
        console.log("8?");
        dispatch(setAnim(false));
        setTimeout(() => {
          setComponentNum(componentNum + 1);
          dispatch(setAnim(true));
        }, 400);
      }
    },
    onSwipedDown: () => {
      if (componentNum !== 0) {
        setAnimationDirectionState("back");
        console.log("0?");
        dispatch(setAnim(false));
        setTimeout(() => {
          setComponentNum(componentNum - 1);
          dispatch(setAnim(true));
        }, 400);
      }
    },
  });

  useEffect(() => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const userID = urlParams.get("user_id");
    const lotId = urlParams.get("lot_id");
    dispatch(getFarmerLot(userID, lotId));
  }, []);

  useEffect(() => {
    if (success) {
      setComponentNum(8);
    } else if (error && componentNum < 3) {
      setComponentNum(3);
    }
  }, [error, componentNum, success]);

  const renderSwitch = (param) => {
    switch (param) {
      case 0:
        return <WelcomePage />;
      case 1:
        return <ImagesInfo />;
      case 2:
        return <MapPage />;
      case 3:
        return <OpinionInfo />;
      case 4:
        return <PersonInfoPage />;
      case 5:
        return <WithWhoPage />;
      case 6:
        return <HobbyPage />;
      case 7:
        return <HappyPage />;
      case 8:
        return <SuccessPage />;
      default:
        return <WelcomePage />;
    }
  };
  return (
    <div {...swipe} className={["App", animationDirectionClass].join(" ")}>
      {renderSwitch(componentNum)}
    </div>
  );
};

export default Layout;
