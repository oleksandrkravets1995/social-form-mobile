export const setTest = (payload) => ({
	payload,
	type: 'SET_TEST',
});

export const setAnim = (payload) => ({
	payload,
	type: 'SET_ANIM'
})

export const UserDataActionCreator = {
	setAge: (payload) => ({ type: 'SET_DATA_AGE', payload }),
	setNationality: (payload) => ({ type: 'SET_DATA_NATIONALITY', payload }),
	setHaveVisited: (payload) => ({ type: 'SET_DATA_HAVE_VISITED', payload }),
	setVisitNum: (payload) => ({ type: 'SET_DATA_VISIT_NUM', payload }),
	setWhithWho: (payload) => ({ type: 'SET_DATA_WITH_WHO', payload }),
	setMostLoved: (payload) => ({ type: 'SET_DATA_MOST_LOVED', payload }),
	setIdeas: (payload) => ({ type: 'SET_DATA_IDEAS', payload }),
	setHappines: (payload) => ({ type: 'SET_DATA_HAPPINES', payload })
}