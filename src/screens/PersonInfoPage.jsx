import React, { useState, useEffect } from "react";
import { CSSTransition } from "react-transition-group";
import { useSelector } from "./../store/index";
import { UserDataActionCreator } from "./../store/test/actions";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";

export const PersonInfoPage = () => {
  const { t } = useTranslation();
  const ageFromRedux = useSelector((state) => state.test.userdata.age);
  const haveVisitedFromRedux = useSelector(
    (state) => state.test.userdata.firstTime
  );
  const [btnId, setBtnId] = useState(0);
  const [age, setAge] = useState(ageFromRedux);
  const active = useSelector((state) => state.test.animation);
  const dispatch = useDispatch();
  useEffect(() => {
    return () => {
      dispatch(UserDataActionCreator.setAge(+age));
    };
  }, [dispatch, age, haveVisitedFromRedux]); // haveVisitedFromRedux ???

  return (
    <div className={["page", "personInfo-page"].join(" ")}>
      <div
        className="container"
        style={{
          display: "flex",
          flexDirection: "column",
          height: "100vh",
          justifyContent: "space-around",
        }}
      >
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <h2>
            {t("few_questions")}
            {/* Fews Questions <pre />
          About you */}
          </h2>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <h2 style={{ marginTop: "60px" }}>{t("how_old")}</h2>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div style={{ textAlign: "center" }} className="inputWrap">
            <input
              type="range"
              min={0}
              max={100}
              onChange={(e) => setAge(e.target.value)}
              value={age}
            />
          </div>
        </CSSTransition>

        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <h3 style={{ color: "#cb9969", textAlign: "center" }}>
            {age} {t("ans")}
          </h3>
        </CSSTransition>

        <h2>{t("been_here")}</h2>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              gap: "10px",
              width: "100%",
            }}
          >
            <div
              style={{ boxShadow: btnId === '1' ? "0px 0px 5px black" : "none" }}
              id="1"
              onClick={(e) => {
                setBtnId(e.target.id);
                dispatch(UserDataActionCreator.setHaveVisited(true));
              }}
              className="section-item"
            >
              {t("oui")}
            </div>
            <div
              id="2"
              style={{ boxShadow: btnId === '2' ? "0px 0px 5px black" : "none" }}
              onClick={(e) => {
                setBtnId(e.target.id);
                dispatch(UserDataActionCreator.setHaveVisited(false));
              }}
              className="section-item"
            >
              {t("non")}
            </div>
          </div>
        </CSSTransition>
      </div>
    </div>
  );
};
