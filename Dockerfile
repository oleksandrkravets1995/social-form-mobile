FROM node:14 AS builder
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm install --production
COPY . .
RUN npm run build

FROM node:14-alpine
RUN npm -g install serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "3000"]
