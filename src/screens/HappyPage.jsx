import React, { useState, useEffect } from "react";
import { useSelector } from "./../store/index";
import { CSSTransition } from "react-transition-group";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { UserDataActionCreator } from "./../store/test/actions";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { saveSurveyData } from "../store/actions";

export const HappyPage = () => {
  const dispatch = useDispatch();
  const [percentage, setPercentage] = useState(50);
  const active = useSelector((state) => state.test.animation);
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(UserDataActionCreator.setHappines(+percentage));
  }, [percentage]);

  return (
    <div className={["page", "happy-page"].join(" ")}>
      <div className="container">
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <h2>{t("how_happy")}</h2>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div className="progressBar">
            <div>
              <CircularProgressbar
                value={percentage}
                text={`${percentage}%`}
                strokeWidth={10}
                counterClockwise={true}
                styles={buildStyles({
                  // Rotation of path and trail, in number of turns (0-1)
                  rotation: 0.55,

                  // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                  strokeLinecap: "butt",

                  // Text size
                  textSize: "14px",

                  // How long animation takes to go from one percentage to another, in seconds
                  pathTransitionDuration: 0.5,

                  // Can specify path transition in more detail, or remove it entirely
                  // pathTransition: 'none',

                  // Colors
                  pathColor: `rgba(88 70 54)`,
                  textColor: "rgba(0, 0, 0)",
                  trailColor: "rgb(253, 244, 234)",
                  backgroundColor: "rgb(253, 244, 234)",
                  border: "1px solid red",
                })}
              />
            </div>
          </div>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div className="inputWrap">
            <input
              type="range"
              onChange={(e) => setPercentage(e.target.value)}
              value={percentage}
            />
          </div>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_right" timeout={300}>
          <div className="smileWrapp">
            <div className="smile-circle">
              <div className="eye-left"></div>
              <div className="eye-right"></div>
              {percentage < 33 && <div className="sad-line"></div>}
              {percentage >= 33 && percentage <= 66 && (
                <div className="line"></div>
              )}
              {percentage > 66 && <div className="happy-line"></div>}
            </div>
          </div>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <div className="buttonWrap">
            <button
              type="button"
              onClick={() => {
                dispatch(saveSurveyData());
              }}
            >
              {t("share_opinion")}
            </button>
          </div>
        </CSSTransition>
        <CSSTransition in={active} classNames="from_left" timeout={300}>
          <div
            className="footer-desc"
            dangerouslySetInnerHTML={{ __html: t("thanks_for_info") }}
          ></div>
        </CSSTransition>
      </div>
    </div>
  );
};
