SHELL := /usr/bin/env bash

DOCKER_IMAGE_NAME := df21
DOCKER_IMAGE_TAG := latest

.PHONY: docker-build
docker-build:
	docker buildx build --tag ${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG} .
